<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TasksController extends Controller
{
    //
    public function index() {

          $task=Task::with('task_importance')->get();

            return view('tasks.index',['task'=>$task]);
    }

    public function show($id) {

            $task=Task::findOrFail($id);
            return view('tasks.show',['task'=>$task]);
    }

    public function create() {
        return view('tasks.create');
    }
    public function store(Task $task) {
        try {
            $task = $this->validate($task);
            Task::create($task);

            return redirect('tasks.index');
        } catch (ValidationException $e) {
            return redirect('/');
        }
    }
    public function edit($id) {
        $task=Task::findOrFail($id);
        return view('tasks.edit',['task'=>$task]);
    }

    public function update(Task $task) {
        $task=$this->validate($task);
        $task->save();

        return view('tasks.index');
    }

    public function destroy($id) {
        Task::destroy($id);
        return view('tasks.index');
    }
}
