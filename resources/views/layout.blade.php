<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task App</title>
</head>
<body>
    @section('content')

    @endsection


    <footer>

        <div class="left">
            This is left footer
        </div>
        <div class="right">
            This is right footer
        </div>
    </footer>
</body>
</html>
