@extends('layout')


@section('content')
    <form action="{{ route('tasks.store') }}" method="POST">
        @csrf

        <div class="list-item">
            <label for="title">Task Title</label>
            <input type="text" name="title" placeholder="Enter a task title">
        </div>
        <div class="list-item">
            <label for="description">Task Description</label>
            <input type="text" name="title" placeholder="Enter a task description">
        </div>
        <div class="list-item">
            <label for="date">Task Due Date</label>
            <input type="date" name="due_date" placeholder="Enter a task due date">
        </div>

        <button>Submit</button>
    </form>
@endsection
