@extends('layout')


@section('content')


    @if(count($tasks)==0)
        <h1>No task found</h1>
        <a href="{{ route('tasks.create') }}">Create One</a>

    @else
        <table>

            <thead>
                <th>
                    Task_id
                </th>
                <th>
                    Task_title
                </th>
                <th>
                    Task_description
                </th>
                <th>
                    Task_due_date
                </th>
            </thead>
            <tbody>

            @foreach($tasks as $task) {
                <tr>
                    <td> {{ $task->id }}</td>
                    <td> {{ $task->title }}</td>
                    <td> {{ $task->description }}</td>
                    <td> {{ $task->due_date }}</td>
                </tr>
            }
            </tbody>
        </table>



        @endforeach


@endsection
