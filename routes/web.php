<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('/tasks')->group(,function () {
   Route::get('/',[\App\Http\Controllers\TasksController::class,'index']);

   Route::get('/{id}',[\App\Http\Controllers\TasksController::class,'show']);

   Route::get('/create',[\App\Http\Controllers\TasksController::class,'create'])->name('tasks.create');

   Route::post('/',[\App\Http\Controllers\TasksController::class,'store']);

   Route::get('/edit',[\App\Http\Controllers\TasksController::class,'edit']);

   Route::put('/',[\App\Http\Controllers\TasksController::class,'update']);

   Route::delete('/',[\App\Http\Controllers\TasksController::class,'destroy']);
});
